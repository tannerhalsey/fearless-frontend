

function alertComponent(){
    return `
    <div id="alert alert-primary" role="alert">
    Something's going wrong!
    </div>
    `
}

function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>

          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
          <p class="card-footer">${startDate} - ${endDate}</p>


        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let i = 1
  
        for (let conference of data.conferences) {
            if (i > 3) {
              i = 1
            }
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const location = details.conference.location.name
            const startDate = Date(details.conference.starts);
            // const startDate = start1.slice(0,15);
            const endDate = Date(details.conference.ends);
            // const endDate = end1.slice(0,15);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            console.log(html);
            const column = document.querySelector(`#col${i}`);
            column.innerHTML += html;
            i++
          }
        }
  
      }
    } catch (e) {
        console.error("You got an error.");
        // Figure out what to do if an error is raised
        const newHTML = alertComponent()
        const somethingWrong = document.querySelector('something-wrong')
        somethingWrong,innerHTML = newHTML;
    }
  
  });