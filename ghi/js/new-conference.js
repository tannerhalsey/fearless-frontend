window.addEventListener('DOMContentLoaded', async () => {
    const locationURL = 'http://localhost:8000/api/locations/';
    const response = await fetch(locationURL);
    if (response.ok) {
        const data = await response.json();
        const locations = data.locations
        let selectTag = document.getElementById('location');
        for (let location of locations) {
            let option = document.createElement("option");
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        const conferencesURL = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferencesURL, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }
    });
});